package com.enterprise.projekt2.controller;

import com.enterprise.projekt2.domain.Person;
import com.enterprise.projekt2.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class Controller {

    @Autowired
    private PersonService ps;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody Person person) {
        ps.create(person);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Mono<Person>> findById(@PathVariable("id") Integer id) {
        Mono<Person> person = ps.findById(id);
        HttpStatus status = person != null ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(person, status);
    }

    @RequestMapping(value = "/findbynick/{nick}", method = RequestMethod.GET)
    public Flux<Person> findByNick(@PathVariable("nick") String nick) {
        return ps.findByNick(nick);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Person> findAll() {
        return ps.findAll();
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public Mono<Person> update(@RequestBody Person f) {
        return ps.update(f);
    }

    @RequestMapping(value = "/del/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") Integer id) {
        ps.delete(id).subscribe();
    }

    @RequestMapping(value = "/capitalised", method = RequestMethod.GET)
    public Mono<Person> Capitilised() {
        return ps.findAll().filter(p -> p.getNick().matches("[A-Z][a-z]*")).next();
    }

    @RequestMapping(value = "/nameEmail", method = RequestMethod.GET)
    public Flux<String> firstId() {
        return ps.findAll().map(p -> p.getNick().concat(" " + p.getEmail() +"\n"));
    }
}
