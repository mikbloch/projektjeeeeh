package com.enterprise.projekt2.service;

import com.enterprise.projekt2.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class PersonService implements IPersonService {
    @Autowired
    PersonRepository pr;

    public void create(Person e) {
        pr.save(e).subscribe();
    }

    public void deleteAll() { pr.deleteAll();}

    public Mono<Person> findById(int id) {
        return pr.findById(id);
    }

    public Flux<Person> findByNick(String nick) {
        return pr.findByNick(nick);
    }

    public Flux<Person> findAll() {
        return pr.findAll();
    }

    public Mono<Person> update(Person e) {
        return pr.save(e);
    }

    public Mono<Void> delete(int id) {
        return pr.deleteById(id);
    }
}
