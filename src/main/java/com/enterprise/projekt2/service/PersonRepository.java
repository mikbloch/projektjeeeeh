package com.enterprise.projekt2.service;


import com.enterprise.projekt2.domain.Person;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

public interface PersonRepository extends ReactiveMongoRepository<Person, Integer> {
    @Query("{ 'nick': ?0 }")
    Flux<Person> findByNick(final String nick);
}
