package com.enterprise.projekt2.service;

import com.enterprise.projekt2.domain.Person;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IPersonService {
    void create(Person e);

    void deleteAll();

    Mono<Person> findById(int id);

    Flux<Person> findByNick(String nick);

    Flux<Person> findAll();

    Mono<Person> update(Person e);

    Mono<Void> delete(int id);
}
