package com.enterprise.projekt2.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.*;
import java.sql.Date;
import java.util.List;

@Data
@Document
@AllArgsConstructor @NoArgsConstructor
public class Person {
    @Id
    private int id;
    private String nick;
    private String password;
    private String email;
}
