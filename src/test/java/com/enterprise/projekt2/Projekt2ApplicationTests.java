package com.enterprise.projekt2;


import com.enterprise.projekt2.controller.Controller;
import com.enterprise.projekt2.domain.Person;
import com.enterprise.projekt2.service.PersonService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import javax.validation.constraints.*;
import java.sql.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Projekt2ApplicationTests {


    @Autowired
    PersonService ps;

    @Autowired
    Controller controller;


    Person j = new Person(1, "aDIn", "35f42f354", "one@wp.pl");
    Person d = new Person(2, "Twa", "tgt3tgf3f42", "two@gmail.com");
    Person t = new Person(3, "TRI", "tr32545f34", "trhee@o2.pl");
    Person c = new Person(4, "CZETEREJ", "tr32545f34", "four@onet.eu");

    Flux<Person> x = Flux.just(j, d, t, c);

    @Test
    public void create() {

    }

}
